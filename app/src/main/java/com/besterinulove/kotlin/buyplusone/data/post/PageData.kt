package com.besterinulove.kotlin.buyplusone.data.post

data class PageData(
    val id: String,
    val posts: Post
)

data class Post(
    val data: List<Data>,
    val paging: Paging
)

data class Data(
    val comments: Comments,
    val id: String,
    val message: String
)

data class Comments(
    val data: List<Message>,
    val paging: ChildPaging
)

data class Message(
    val from: From,
    val id: String,
    val message: String
)

data class From(
    val id: String,
    val name: String
)

data class ChildPaging(
    val cursors: ChildCursors
)

data class ChildCursors(
    val after: String,
    val before: String
)

data class Paging(
    val cursors: Cursors
)

data class Cursors(
    val after: String,
    val before: String
)