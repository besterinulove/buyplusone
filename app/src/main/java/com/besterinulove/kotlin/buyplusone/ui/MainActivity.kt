package com.besterinulove.kotlin.buyplusone.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.besterinulove.kotlin.buyplusone.login.LoginManager
import com.besterinulove.kotlin.buyplusone.R
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: FacebookUserViewModel
    private val TAG = MainActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(FacebookUserViewModel::class.java)
        setUpLoginButton()

        if (LoginManager.isLoggedIn()) {
            viewModel.loadUser()
        }
    }

    private fun setUpLoginButton() {
        login_button.apply {
            setPermissions(LoginManager.permissions)
            registerCallback(
                LoginManager.callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(result: LoginResult?) {
                        Log.d(TAG, "onSuccess: ")
                    }

                    override fun onCancel() {
                        Log.d(TAG, "onCancel: ")
                    }

                    override fun onError(error: FacebookException?) {
                        Log.d(TAG, "onError: ")
                    }
                })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        LoginManager.callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }
}
