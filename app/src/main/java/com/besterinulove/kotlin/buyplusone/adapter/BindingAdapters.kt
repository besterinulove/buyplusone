package com.besterinulove.kotlin.buyplusone.adapter

import android.util.Log
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.besterinulove.kotlin.buyplusone.data.page.Page
import com.besterinulove.kotlin.buyplusone.data.post.Data
import com.besterinulove.kotlin.buyplusone.data.post.Message

@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, datas: List<Any>?) {
//    adapter.submitList(datas)
    Log.d("bindRecyclerView", "bindRecyclerView: ${datas?.size}\t$datas")
    datas?.let {
        when (datas[0]) {
            is Page -> {
                val adapter = recyclerView.adapter as UserAdapter<Page>
                adapter.submitList(datas as List<Page>)
            }
            is Data->{
                val adapter = recyclerView.adapter as UserAdapter<Data>
                adapter.submitList(datas as List<Data>)
            }
            is Message->{
                val adapter = recyclerView.adapter as UserAdapter<Message>
                adapter.submitList(datas as List<Message>)
            }
        }
    }
}