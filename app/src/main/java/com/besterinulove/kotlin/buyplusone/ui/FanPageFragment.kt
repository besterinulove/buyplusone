package com.besterinulove.kotlin.buyplusone.ui


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.besterinulove.kotlin.buyplusone.R
import com.besterinulove.kotlin.buyplusone.adapter.UserAdapter
import com.besterinulove.kotlin.buyplusone.data.page.Page
import com.besterinulove.kotlin.buyplusone.databinding.FragmentFanPageBinding

/**
 * A simple [Fragment] subclass.
 */
class FanPageFragment : Fragment() {
    private val TAG= FanPageFragment::class.java.simpleName

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentFanPageBinding.inflate(inflater)
        val viewModel = ViewModelProviders.of(activity!!).get(FacebookUserViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        binding.recycler.adapter = UserAdapter<Page>(UserAdapter.OnClickListener {
            viewModel.setNavigateToPost(it)
        })
        viewModel.onNavigation.observe(this, Observer {
            if (it) {
                findNavController().navigate(R.id.action_fanPageFragment_to_fanPostFragment)
                viewModel.doneNavigation()
            }
        })
        return binding.root
    }


}
