package com.besterinulove.kotlin.buyplusone.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.besterinulove.kotlin.buyplusone.data.page.Page
import com.besterinulove.kotlin.buyplusone.data.post.Data
import com.besterinulove.kotlin.buyplusone.data.post.Message
import com.besterinulove.kotlin.buyplusone.databinding.RowItemBinding

class UserAdapter<T>(private val onClickListener: OnClickListener<T>?) :
    ListAdapter<T, UserAdapter.UserViewHolder<T>>(FanPageDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder<T> {
        return UserViewHolder(RowItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: UserViewHolder<T>, position: Int) {
        val data = getItem(position)
        onClickListener?.let {
            holder.itemView.setOnClickListener {
                onClickListener.onClick(data)
            }
        }
        holder.bind(data)
    }

    class UserViewHolder<T>(private var binding: RowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(t: T) {
            when (t) {
                is Page -> binding.textView.text = t.name
                is Data -> binding.textView.text = t.message
                is Message -> binding.textView.text ="${t.from?.name} : ${t.message}"
            }
        }
    }

    class FanPageDiffCallback<T> : DiffUtil.ItemCallback<T>() {
        override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
            if (oldItem is Page && newItem is Page) {
                return oldItem.id == newItem.id
            }
            if (oldItem is Data && newItem is Data) {
                return oldItem.message == newItem.message
            }
            return false
        }
    }

    class OnClickListener<T>(val clickListener: (T) -> Unit) {
        fun onClick(t: T) = clickListener(t)
    }
}