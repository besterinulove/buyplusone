package com.besterinulove.kotlin.buyplusone.ui


//import com.besterinulove.kotlin.buyplusone.adapter.UserAdapter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.besterinulove.kotlin.buyplusone.R
import com.besterinulove.kotlin.buyplusone.adapter.UserAdapter
import com.besterinulove.kotlin.buyplusone.data.post.Data
import com.besterinulove.kotlin.buyplusone.databinding.FragmentFanPostBinding


/**
 * A simple [Fragment] subclass.
 */
class FanPostFragment : Fragment() {
    val TAG = FanPostFragment::class.java.simpleName

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentFanPostBinding.inflate(inflater)
        val viewModel = ViewModelProviders.of(activity!!).get(FacebookUserViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        binding.recycler.adapter = UserAdapter<Data>(UserAdapter.OnClickListener {
            viewModel.setNavigateToMessage(it)
        })
        viewModel.onNavigation.observe(this, Observer {
            if (it){
                findNavController().navigate(R.id.action_fanPostFragment_to_postMessageFragment)
                viewModel.doneNavigation()
            }
        })
        return binding.root
    }
}
