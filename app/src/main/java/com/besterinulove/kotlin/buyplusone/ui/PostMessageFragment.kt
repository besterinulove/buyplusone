package com.besterinulove.kotlin.buyplusone.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.besterinulove.kotlin.buyplusone.adapter.UserAdapter
import com.besterinulove.kotlin.buyplusone.data.post.Message
import com.besterinulove.kotlin.buyplusone.databinding.FragmentPostMessageBinding

/**
 * A simple [Fragment] subclass.
 */
class PostMessageFragment : Fragment() {
    val TAG = PostMessageFragment::class.java.simpleName

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentPostMessageBinding.inflate(inflater)
        val viewModel = ViewModelProviders.of(activity!!).get(FacebookUserViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        binding.recycler.adapter = UserAdapter<Message>(null)
        return binding.root
    }


}
