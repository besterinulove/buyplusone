package com.besterinulove.kotlin.buyplusone.ui


import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.besterinulove.kotlin.buyplusone.data.page.Page
import com.besterinulove.kotlin.buyplusone.data.page.UserData
import com.besterinulove.kotlin.buyplusone.data.post.Data
import com.besterinulove.kotlin.buyplusone.data.post.Message
import com.besterinulove.kotlin.buyplusone.data.post.PageData
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.GraphResponse
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FacebookUserViewModel : ViewModel() {
    private lateinit var response: GraphResponse
    private val TAG = FacebookUserViewModel::class.java.simpleName
    private var _fanPages = MutableLiveData<List<Page>>()
    val fanPages: LiveData<List<Page>>
        get() = _fanPages
    private var _fanPosts = MutableLiveData<List<Data>>()
    val fanPosts: LiveData<List<Data>>
        get() = _fanPosts
    private var _fanMessages = MutableLiveData<List<Message>>()
    val fanMessages: LiveData<List<Message>>
        get() = _fanMessages

    private var _onNavigation = MutableLiveData<Boolean>()
    val onNavigation: LiveData<Boolean>
        get() = _onNavigation

    val gson = Gson()

    fun loadUser() {
        viewModelScope.launch {
            response = getResponse()
            val pages = gson.fromJson(response.jsonObject.toString(), UserData::class.java)
            _fanPages.value = pages.data
        }
    }

    private suspend fun getResponse(
        accessToken: AccessToken = AccessToken.getCurrentAccessToken()
        , graphPath: String = "/me/accounts", parameters: Bundle? = null
    ): GraphResponse {
        return withContext(Dispatchers.IO) {
            val request = GraphRequest.newGraphPathRequest(
                accessToken,
                graphPath,
                null
            )
            parameters?.let {
                request.parameters = it
            }
            request.executeAndWait()
        }
    }

    fun setNavigateToPost(page: Page) {
        viewModelScope.launch {
            val parameters = Bundle()
            parameters.putString(
                "access_token",
                page.access_token
            )
            val response = getResponse(
                graphPath = "/${page.id}/?fields=posts{message,comments{from,message}}",
                parameters = parameters
            )
            val pageData =
                gson.fromJson<PageData>(response.jsonObject.toString(), PageData::class.java)
            _fanPosts.value = pageData.posts.data.filter { it.message != null }
            _onNavigation.value = true
        }
    }

    fun doneNavigation() {
        _onNavigation.value = false
    }

    fun setNavigateToMessage(data: Data) {
        _onNavigation.value = true
        _fanMessages.value = data.comments.data.filter {
            val regex: Regex = Regex("\\d+")
            it.message.matches(regex)
        }
    }
}