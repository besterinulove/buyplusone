package com.besterinulove.kotlin.buyplusone.login

import com.facebook.AccessToken
import com.facebook.CallbackManager

class LoginManager {
    companion object {
        val permissions = mutableListOf(
            "email",
            "manage_pages",
            "pages_show_list",
            "publish_pages",
            "read_page_mailboxes",
            " pages_manage_cta"
        )

        val callbackManager = CallbackManager.Factory.create()!!
        private val currentAccessToken: AccessToken? = AccessToken.getCurrentAccessToken()

        fun isLoggedIn(): Boolean {
            return currentAccessToken != null && !currentAccessToken.isExpired
        }
    }
}