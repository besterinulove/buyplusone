package com.besterinulove.kotlin.buyplusone.data.page

data class UserData(
    val data: List<Page>,
    val paging: Paging
)

data class Page(
    val access_token: String,
    val category: String,
    val category_list: List<Category>,
    val id: String,
    val name: String,
    val tasks: List<String>
)

data class Category(
    val id: String,
    val name: String
)

data class Paging(
    val cursors: Cursors
)

data class Cursors(
    val after: String,
    val before: String
)